# Lights in

[**Lights out**](https://en.wikipedia.org/wiki/Lights_Out_(game)) is a puzzle
game consisting in a grid of lights that can be on or off. Pressing any of these
lights toggles it and all other lights that are adjacent to it horizontally or
vertically (so pressing an interior light will toggle four other lights and
pressing a corner light will only toggle two others). The objective of the game
is to get all the lights to be switched off, ideally in the minimum number of
presses.

The original version had a $`5 \times 5`$ grid, but it can be generalized to an
arbitrary $`n \times m`$ grid. Remarkably, for different values of $`n`$ and
$`m`$ there may be some unreachable states (or, equivalently, _unsolvable_
states) while for other values of $`n`$ and $`m`$ there will always be a
(unique) solution.

**Lights in** is a simple program to solve any (solvable) Lights out board in as
few button presses as possible. It relies on
[SageMath](https://github.com/sagemath/sage) to actually solve the puzzle and
provides a Tcl/Tk user interface.

## Mathematics
Each light can be represented as a 1 or a 0, depending on whether it's on or
off: this way, the state of any $`n \times m`$ grid can be encoded as a
$`nm`$-vector made up of 0's and 1's by indexing the matrix rows first, so a $`3
\times 3`$ grid is indexed as

$$
\begin{pmatrix}
  1 & 2 & 3 \\
  4 & 5 & 6 \\
  7 & 8 & 9
\end{pmatrix}.
$$

Similarly, any combination of button presses can be represented as a
$`nm`$-vector depending on whether each individual button is pressed or
not. Since pressing any button twice is equivalent to not pressing it at all, it
is natural to perform the calculations modulo 2; this way two combinations of
button presses can be added component-wise to yield the corresponding
combination. Defining scalar multiplication in the natural way we can treat the
set of all combinations of button presses as a vector space of dimension $`nm`$
over the field $`\Z/2\Z`$; we take $`V := (\Z/2\Z)^{nm}`$ to be this vector
space.

We can give the same structure to the space of board configurations (since
toggling a light twice is the same as doing nothing), so we also take $`W :=
(\Z/2\Z)^{nm}`$. This way, the mapping $`V \to W`$ turns out to be linear and can
be represented by a $`nm \times nm`$ matrix; the columns of this matrix are
obtained below.

When pressing the $`i`$th button, the lights that are toggled are:
- the $`i`$th light itself, so there must be a 1 at position $`(i,i)`$
- the lights to the left and to the right, so there must be a 1 at positions
  $`(i,i-1)`$ and $`(i,i+1)`$ provided that $`(i-1)`$ and $`(i+1)`$ lie inside
  the grid and are in the same row as $`i`$
- the lights above and below, so there must be a 1 at positions $`(i,i-m)`$ and
  $`(i,i+m)`$, provided that these lie inside the grid

Summing up, the matrix can be written as a block matrix in the following way:

$$
M =
\begin{pmatrix}
  B & I      &        &        &        &   \\
  I & B      & I      &        &        &   \\
    & \ddots & \ddots & \ddots &        &   \\
    &        & \ddots & \ddots & \ddots &   \\
    &        &        & I      & B      & I \\
    &        &        &        & I      & B
\end{pmatrix}
$$

where each row and column contains $`n`$ blocks, $`I`$ is the $`m \times m`$
identity matrix and $`B`$ is the $`m \times m`$ matrix defined as

$$
B =
\begin{pmatrix}
  1 & 1      &        &        &        &   \\
  1 & 1      & 1      &        &        &   \\
    & \ddots & \ddots & \ddots &        &   \\
    &        & \ddots & \ddots & \ddots &   \\
    &        &        & 1      & 1      & 1 \\
    &        &        &        & 1      & 1
\end{pmatrix}.
$$

This way, given an initial board configuration $`c`$, the Lights out puzzle can
be reduced to solving the system of equations $`Mx = c`$ over $`\Z/2\Z`$.

As an example, when $`n = m = 3`$ this matrix takes the form

$$
\begin{pmatrix}
  1 & 1 & 0 & 1 &   &   &   &   &   \\
  1 & 1 & 1 &   & 1 &   &   &   &   \\
  0 & 1 & 1 &   &   & 1 &   &   &   \\
  1 &   &   & 1 & 1 & 0 & 1 &   &   \\
    & 1 &   & 1 & 1 & 1 &   & 1 &   \\
    &   & 1 & 0 & 1 & 1 &   &   & 1 \\
	&   &   & 1 &   &   & 1 & 1 & 0 \\
    &   &   &   & 1 &   & 1 & 1 & 1 \\
    &   &   &   &   & 1 & 0 & 1 & 1
\end{pmatrix}
$$

One may wonder if this matrix is regular; unfortunately, the answer is no, in
general. For instance, in the original game (with a $`5 \times 5`$ grid) the
resulting $`25 \times 25`$ matrix has a kernel of dimension $`2`$, and therefore
it contains $`2^2 = 4`$ vectors. The First Isomorphism Theorem states that

$$
\mathrm{im}(M) \simeq V/\ker(M),
$$

which means that only a fourth of all possible configurations has a solution
and, in fact, every solvable configuration has exactly four solutions.

However, for some values of $`n, m`$ this matrix _is_ regular, so every board
configuration has a single solution.

In general, the solution space of a non-homogeneous system of linear equations
is the affine space $`s + \ker(M)`$, with $`s`$ being any solution to the
system. Since $`|\ker(M)| = 2^{\dim \ker(M)}`$, we only need to check a finite
number of solutions to find the one with the minimal number of 1's, i.e. minimal
number of button presses.

This is discussed in more detail in David Joyner's [Adventures in Group
Theory](https://press.jhu.edu/books/title/9554/adventures-group-theory), along
with many other puzzles.
