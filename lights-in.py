from sage.rings.finite_rings.finite_field_constructor import FiniteField
from sage.matrix.constructor import matrix, block_diagonal_matrix
from sage.modules.free_module_element import vector
from sage.misc.misc import BackslashOperator

from itertools import product

from tkinter import (Tk, Frame, Button)

# Logic #######################################################################

nrows = 5
ncols = 5

F2 = FiniteField(2)

B = matrix(F2, ncols, ncols, lambda i, j: abs(i-j) <= 1)
M = block_diagonal_matrix([B] * nrows)
M += matrix(F2, nrows*ncols, nrows*ncols, lambda i, j: abs(i-j) == ncols)

base = M.kernel().gens()
dim = len(base)


def sol(v, is_matrix=False):
    if is_matrix:
        v = v.list()

    v = vector(F2, v) # convert it to a vector over F2

    solution = M * BackslashOperator() * v     # actually solve the system
    solution = simplify_solution(solution)
    return matrix(F2, nrows, ncols, solution)  # return solution in matrix form


def simplify_solution(solution):
    best_solution = solution
    best_weight = best_solution.hamming_weight()

    # the whole solution space is obtained from a solution ‘s’ as ‘s + ker(M)’
    for coords in product([0,1], repeat=dim):
        current_solution = solution + from_coordinates(coords)
        if current_solution.hamming_weight() < best_weight:
            best_solution = current_solution

    return best_solution


def from_coordinates(coords):
    return sum([coord * v for (coord, v) in zip(coords, base)])


# User interface ##############################################################

root = Tk()
frame = Frame(root)
frame.grid()
buttons = []


def toggle(row, col):
    if buttons[row][col]["relief"] == "raised":
        buttons[row][col]["relief"] = "sunken"
        buttons[row][col]["bg"] = "cyan"
    else:
        buttons[row][col]["relief"] = "raised"
        buttons[row][col]["bg"] = "#d9d9d9"


def solve_function():
    v = []

    # get user input:
    for row in range(nrows):
        for col in range(ncols):
            if buttons[row][col]["relief"] == "raised":
                v.append(0)
            elif buttons[row][col]["relief"] == "sunken":
                v.append(1)

    # actually solve the puzzle:
    solution = sol(v)

    # print the optimal solution:
    for row in range(nrows):
        for col in range(ncols):
            if solution[row, col] == 1:
                buttons[row][col]["bg"] = "yellow"


def clear_function():
    for row in range(nrows):
        for col in range(ncols):
            buttons[row][col].configure(bg="#d9d9d9", relief="raised")


# Actually create the GUI:

for row in range(nrows):
    buttons.append([])
    for col in range(ncols):
        buttons[row].append(Button(frame,
                                   command=lambda row=row, col=col: toggle(row,col),
                                   bg="#d8d9d9",
                                   relief="raised"))
        buttons[row][col].grid(row=row, column=col)

solve_button = Button(frame, command=lambda: solve_function(), text="Solve!")
solve_button.grid(row=nrows, column=ncols)

clear_button = Button(frame, command=lambda: clear_function(), text="Clear")
clear_button.grid(row=nrows+1, column=ncols)

root.mainloop()
